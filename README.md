# MS Teasm WebSocket API client

- Build with `./build.sh`
- Install by unzipping into ~/.hammerspoon/Spoons
- Use like this:

  1. Obtain an API key from Teams Settings -> Privacy -> API (at the bottom)
  2. Add to Hammerspoon's config file something like this:
  ```lua
  local teams = hs.loadSpoon("TeamsWS")
  teams:setApiKey("<your-api-key>")
  teams:bindHotKeys({
    toggleMute = { {}, "f13" },
    toggleVideo = { {}, "f14" },
    leave = { { "cmd" }, "f13" },
    reactLike = { { }, "pad-" },
    reactLaugh = { { }, "pad*" },
    reactApplause = { { }, "pad/" }
  })

  -- Optionally you can publish the meeting state to a custom endpoint, e.g. to integrate with some actuator ("On Air" light above the office door, etc.)
  teams:setMeetingStateCallback(function(inMeeting)
    hs.http.post("http://my.custom.automation.endpoint", "inMeeting=" .. (inMeeting and "true" or "false"), nil)
  end)
  teams:start()
  ```
  3. Enjoy
