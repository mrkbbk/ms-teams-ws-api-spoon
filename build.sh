#!/bin/bash

set -e

DIST="$(mktemp -d)"
trap 'rm -rf $DIST' EXIT

mkdir "$DIST/TeamsWS.spoon"
cp -r init.lua "$DIST/TeamsWS.spoon/init.lua"
pushd "$DIST"
zip -r TeamsWS.spoon.zip TeamsWS.spoon
popd
mv "$DIST/TeamsWS.spoon.zip" .
