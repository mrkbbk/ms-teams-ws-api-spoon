local logger = hs.logger.new("TeamsWS", "debug")

local obj = {
  apiKey = nil,
  ws = {},
  status = {},
  previousOnlineStatus = nil,
  onlineCallback = nil
}
obj.__index = obj

local TEAMS_WS_ENDPOINT = "ws://localhost:8124?token=##APIKEY##&protocol-version=1.0.0&manufacturer=MuteDeck&device=MuteDeck&app=MuteDeck&app-version=1.4"

--@param apiKey string MS Teasm Websocket API key
function obj:setApiKey(apiKey)
  self.apiKey = apiKey
  return self
end

function obj:start()
  self:connect()
  while self.ws:status() ~= "open" do
    logger:d("Waiting for socket to open")
    hs.timer.usleep(100000)
    self:getStatus()
  end
end

function obj:connect()
  local url = TEAMS_WS_ENDPOINT:gsub("##APIKEY##", self.apiKey)
  self.ws = hs.websocket.new(url, function (mode, msg) self:socketHandler(mode, msg) end)
  return self
end

--@param mode string Socket mode (open, closed, fail)
--@param msg string Message received
--@return void
function obj:socketHandler(mode, msg)
  if mode == "closed" then
    hs.timer.doAfter(1, function() self:connect() end)
  elseif mode == "fail" then
    logger:d("Socket failed")
    hs.timer.doAfter(1, function() self:connect() end)
  elseif mode == "open" then
    return
  else
    self:handleStatusResponse(msg)
  end
end

--@param service string
--@param action string
--@param payload table optional whole payload table
--@return void
function obj:send(service, action, payload)
  local pl
  if not payload then
    pl = {
      apiVersion="1.0.0",
      service=service,
      action=action,
      manufacturer="mrkbbk",
      device="ImaginaryDeck",
    }
  else
    pl = payload
  end

  pl.timestamp = hs.timer.secondsSinceEpoch() * 1000
  local msg = hs.json.encode(pl)
  if self.ws:status() == "open" then
    self.ws:send(msg)
  else
    logger:d("Socket not open, not sending ", msg)
    return
  end
end

--@param msg string Status message as JSON string
--@return void
function obj:handleStatusResponse(msg)
  self.status = hs.json.decode(msg)["meetingUpdate"]
  local inMeeting = self.status.meetingState.isInMeeting
  if obj.onlineCallback then
    if inMeeting ~= self.previousOnlineStatus then
      self.previousOnlineStatus = inMeeting
      obj.onlineCallback(inMeeting)
    end
  end
  -- wait a second and get status again
  hs.timer.doAfter(1, function() self:getStatus() end)
end

function obj:getStatus()
  self:send("query-meeting-state", "query-meeting-state")
end

--@param what string Permission to check
--@return boolean
function obj:canI(what)
  if self.status and self.status.meetingPermissions then
    return self.status.meetingPermissions[what]
  end
  return false
end

function obj:toggleMute()
  if self:canI("canToggleMute") then
    self:send("toggle-mute", "toggle-mute")
  end
end

function obj:toggleVideo()
  if self:canI("canToggleVideo") then
    self:send("toggle-video", "toggle-video")
  end
end

function obj:leave()
  if self:canI("canLeave") then
    self:send("leave-call", "leave-call")
  end
end

function obj:reactApplause()
  if self:canI("canReact") then
    self:send("call", "react-applause")
  end
end

function obj:reactLaugh()
  if self:canI("canReact") then
    self:send("call", "react-laugh")
  end
end

function obj:reactLike()
  if self:canI("canReact") then
    self:send("call", "react-like")
  end
end

function obj:reactLove()
  if self:canI("canReact") then
    self:send("call", "react-love")
  end
end

function obj:reactWow()
  if self:canI("canReact") then
    self:send("call", "react-wow")
  end
end

--@param mapping table Hotkey mapping
function obj:bindHotKeys(mapping)
  local def = {
    toggleMute = hs.fnutils.partial(self.toggleMute, self),
    toggleVideo = hs.fnutils.partial(self.toggleVideo, self),
    leave = hs.fnutils.partial(self.leave, self),
    reactApplause = hs.fnutils.partial(self.reactApplause, self),
    reactLaugh = hs.fnutils.partial(self.reactLaugh, self),
    reactLike = hs.fnutils.partial(self.reactLike, self),
    reactLove = hs.fnutils.partial(self.reactLove, self),
    reactWow = hs.fnutils.partial(self.reactWow, self)
  }

  -- TODO: add "push to unmute" ability (temporary unmute while holding a key)
  hs.spoons.bindHotkeysToSpec(def, mapping)
end

--@param fn function Callback function to call when meeting state changes
function obj:setMeetingStateCallback(fn)
  obj.onlineCallback = fn
end

return obj
